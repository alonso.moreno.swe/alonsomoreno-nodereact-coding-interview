type Gender =
  | "All"
  | "Male"
  | "Female"
  | "Non-Binary"
  | "Agender"
  | "Genderfluid"
  | "Genderqueer"
  | "Bigender"
  | "Polygender"
  | null;
