import axios from "axios";
import { IUserProps } from "../dtos/user.dto";

type QueryUserParams = {
  gender: Gender;
  queryString: string;
};

export class BackendClient {
  private readonly baseUrl: string;

  constructor(baseUrl = "http://localhost:3001/v1") {
    this.baseUrl = baseUrl;
  }

  async getAllUsers(): Promise<{ data: IUserProps[] }> {
    return (await axios.get(`${this.baseUrl}/people/all`, {})).data;
  }

  async getUsersByFilter(
    params: QueryUserParams
  ): Promise<{ data: IUserProps[] }> {

    console.log(`?gender=${params.gender}&queryString=${params.queryString}`);
    return (
      await axios.get(
        `${this.baseUrl}/people?gender=${params.gender}&queryString=${params.queryString}`,
        {}
      )
    ).data;
  }
}
