import React, { FC, useState, useEffect } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress } from "@mui/material";

import { BackendClient } from "../clients/backend.client";
import { Button, Input, MenuItem, Select, TextField } from "@material-ui/core";

const GENDER = [
  "All",
  "Male",
  "Female",
  "Non-Binary",
  "Agender",
  "Genderfluid",
  "Genderqueer",
  "Bigender",
  "Polygender",
];

const backendClient = new BackendClient();

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [loading, setIsLoading] = useState<boolean>(true);
  const [gender, setGender] = useState<Gender>(null);
  const [query, setQuery] = useState<string>("");

  useEffect(() => {
    setIsLoading(true);

    (async () => {
      const result = await backendClient.getAllUsers();
      setUsers(result.data);
      setIsLoading(false);
    })();
  }, []);

  const handleOnClick = () => {
    (async () => {
      setIsLoading(true);

      const result = await backendClient.getUsersByFilter({
        gender,
        queryString: query,
      });

      setUsers(result.data);
      setIsLoading(false);
    })();
  };


  return (
    <div style={{ paddingTop: "30px" }}>
      <Select
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        value={gender}
        label="Age"
        onChange={(event) => {
          setGender(event.target.value as Gender);
        }}
      >
        {GENDER.map((el) => (
          <MenuItem value={el}>{el}</MenuItem>
        ))}
      </Select>
      <TextField
        id="query-text"
        label="Text"
        variant="outlined"
        onChange={(event) => setQuery(event.target.value)}
      />
      <Button variant="contained" onClick={handleOnClick}>
        Search
      </Button>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        {loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <div>
            {users.length
              ? users.map((user) => {
                  return <UserCard key={user.id} {...user} />;
                })
              : null}
          </div>
        )}
      </div>
    </div>
  );
};
