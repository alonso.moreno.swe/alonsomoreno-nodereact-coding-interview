import {
  JsonController,
  Get,
  HttpCode,
  NotFoundError,
  Param,
  QueryParams,
} from "routing-controllers";
import { PeopleProcessing } from "../services/people_processing.service";
import { IsString } from "class-validator";

const peopleProcessing = new PeopleProcessing();

class GetByFilterQuery {
  @IsString()
  queryString: string;

  @IsString()
  gender:
    | "All"
    | "Male"
    | "Female"
    | "Non-Binary"
    | "Agender"
    | "Genderfluid"
    | "Genderqueer"
    | "Bigender"
    | "Polygender";
}

@JsonController("/people", { transformResponse: false })
export default class PeopleController {
  @HttpCode(200)
  @Get("/all")
  getAllPeople() {
    const people = peopleProcessing.getAll();

    if (!people) {
      throw new NotFoundError("No people found");
    }

    return {
      data: people,
    };
  }

  @HttpCode(200)
  @Get("/:id")
  getPerson(@Param("id") id: number) {
    const person = peopleProcessing.getById(id);

    if (!person) {
      throw new NotFoundError("No person found");
    }

    return {
      data: person,
    };
  }

  @HttpCode(200)
  @Get("/")
  getByFilters(@QueryParams() query: GetByFilterQuery) {

    const person = peopleProcessing.getByFilters({
      gender: query.gender,
      queryString: query.queryString,
    });

    if (!person) {
      throw new NotFoundError("No person found");
    }

    return {
      data: person,
    };
  }
}
