import people_data from "../data/people_data.json";

type Filters = {
  gender?:
    | "All"
    | "Male"
    | "Female"
    | "Non-Binary"
    | "Agender"
    | "Genderfluid"
    | "Genderqueer"
    | "Bigender"
    | "Polygender";
  queryString: string;
};

export class PeopleProcessing {
  getById(id: number) {
    return people_data.find((p) => p.id === id);
  }

  getByFilters(filters: Filters) {
    let gender = filters.gender || "";

    if (gender === "All") {
      gender = "";
    }

    const query = filters.queryString || "";

    let genderedFiltered = [...people_data];

    if (gender) {
      genderedFiltered = people_data.filter((el) => el.gender.includes(gender));
    }

    return genderedFiltered.filter((el) => {
      if (el.title?.includes(query) || el.company.includes(query)) {
        return el;
      }
    });
  }

  getAll() {
    return people_data;
  }
}
